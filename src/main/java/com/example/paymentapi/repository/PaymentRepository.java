package com.example.paymentapi.repository;

import com.example.paymentapi.entity.Payment;

import java.util.List;

public interface PaymentRepository {
    int rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
}
