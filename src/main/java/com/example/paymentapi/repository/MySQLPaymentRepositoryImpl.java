package com.example.paymentapi.repository;

import com.example.paymentapi.entity.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class MySQLPaymentRepositoryImpl implements PaymentRepository {

    @Autowired
    JdbcTemplate template;

    @Override
    public int rowCount() {
        String sql = "select count(*) from payment";
        return template.queryForObject(sql, Integer.class);
    }

    @Override
    public Payment findById(int id) {
        String sql = "select * from payment where id=?";
        return template.queryForObject(sql, new PaymentRowMapper(), id);
    }

    @Override
    public List<Payment> findByType(String type) {
        String sql = "select * from payment where type=?";
        return template.query(sql, new PaymentRowMapper(), type);
    }

    @Override
    public int save(Payment payment) {
        String sql = "insert into payment(id, paymentdate, type, amount, custid) values(?,?,?,?,?)";
        template.update(sql, payment.getId(), payment.getPaymentDate(), payment.getType(), payment.getAmount(), payment.getCustid());
        return 1;
    }
}

class PaymentRowMapper implements RowMapper<Payment> {

    @Override
    public Payment mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Payment(rs.getInt("id"), rs.getDate("paymentdate"), rs.getString("type"), rs.getDouble("amount"), rs.getInt("Custid"));
    }
}